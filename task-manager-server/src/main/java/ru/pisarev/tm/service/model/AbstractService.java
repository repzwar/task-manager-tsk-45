package ru.pisarev.tm.service.model;

import org.jetbrains.annotations.NotNull;
import ru.pisarev.tm.api.service.IConnectionService;
import ru.pisarev.tm.model.AbstractEntity;

public abstract class AbstractService<E extends AbstractEntity> {

    @NotNull
    protected final IConnectionService connectionService;

    @NotNull
    public AbstractService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

}
