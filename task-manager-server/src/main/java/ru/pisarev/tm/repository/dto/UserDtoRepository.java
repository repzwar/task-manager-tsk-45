package ru.pisarev.tm.repository.dto;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.api.repository.dto.IUserDtoRepository;
import ru.pisarev.tm.dto.UserDto;

import javax.persistence.EntityManager;
import java.util.List;

public final class UserDtoRepository extends AbstractDtoRepository<UserDto> implements IUserDtoRepository {

    public UserDtoRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    public List<UserDto> findAll() {
        return entityManager.createQuery("SELECT e FROM UserDto e", UserDto.class).getResultList();
    }

    public UserDto findById(@Nullable final String id) {
        return entityManager.find(UserDto.class, id);
    }

    public void clear() {
        entityManager
                .createQuery("DELETE FROM UserDto e")
                .executeUpdate();
    }

    public void removeById(@Nullable final String id) {
        UserDto reference = entityManager.getReference(UserDto.class, id);
        entityManager.remove(reference);
    }

    @Nullable
    @Override
    public UserDto findByLogin(@Nullable final String login) {
        return getSingleResult(
                entityManager
                        .createQuery("SELECT e FROM UserDto e WHERE e.login = :login", UserDto.class)
                        .setHint(QueryHints.HINT_CACHEABLE, true)
                        .setParameter("login", login)
                        .setMaxResults(1)
        );
    }

    @Nullable
    @Override
    public UserDto findByEmail(@Nullable final String email) {
        return getSingleResult(
                entityManager
                        .createQuery("SELECT e FROM UserDto e WHERE e.email = :email", UserDto.class)
                        .setHint(QueryHints.HINT_CACHEABLE, true)
                        .setParameter("email", email)
                        .setMaxResults(1)
        );
    }

    @Override
    public void removeUserByLogin(@Nullable final String login) {
        entityManager
                .createQuery("DELETE FROM UserDto e WHERE e.login = :login")
                .setParameter(login, login)
                .executeUpdate();
    }

}